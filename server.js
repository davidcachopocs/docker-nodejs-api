'use strict';

require('./env/env');

const express = require('express'),
  router = require('./router'),
	logger = require('./config/logger').appLogger;

process.on('uncaughtException', (error) => {
	logger.error(`UNCAUGHTEXCEPTION --> ${error}`);
});

process.on('exit', (code) => {
	switch(code) {
		/*case -1:
			logger.error(`App stopped because of xxxx`);
			break;*/
		default:
			if(code < 0) {
				logger.error(`Calculator stopped because of unknown error (code: ${code})`);
			} else {
				logger.warn(`Calculator stopped`);
			}
	}
});

const app = express();

app.use(router);

app.listen(process.env.PORT, () => {
  logger.info(`Api ready on port ${process.env.PORT}`);
});
