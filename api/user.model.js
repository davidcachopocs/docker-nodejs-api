'user strict';

const undefinedUser = new Error('User undefined');
const invalidUser = new Error('User must contains name and email');

exports.checkUser = (user, callback) => {
	if(!user) {
		callback(undefinedUser);
	} else if (!user.name || !user.email) {
		callback(invalidUser);
	}

	let validUser = {
		id: user.id ? user.id : 0,
		name: user.name,
		email: user.email
	}

	callback(null, validUser);
}