'use strict';

const repository = require('./users.repository');

exports.getUsers = getUsersFn;
exports.addUser = addUserFn;
exports.updateUser = updateUserFn;
exports.deleteUser = deleteUsersFn;

function getUsersFn(request, response) {
	let query = {};
	if(request.params.id) {
		query.id = Number(request.params.id);
	} else {
		query = request.query;
	}

	repository.select(query, (error, result) => {
		if(error) {
			response.status(400).send(error);
		}

		response.status(200).send(result);
	});
}

function addUserFn(request, response) {
	repository.save(request.body, (error, result) => {
		if(error) {
			response.status(400).send(error);
		}

		response.sendStatus(201);
	});
}

function updateUserFn(request, response) {
	if(!request.params.id) {
		response.status(400).send('Identifier not defined');
	}

	repository.update(request.params.id, request.body, (error, result) => {
		if(error) {
			response.status(400).send(error);
		}

		response.sendStatus(200);
	});
}

function deleteUsersFn(request, response) {
	if(!request.params.id) {
		response.status(400).send('Identifier not defined');
	}

	repository.delete(request.params.id, (error) => {
		if(error) {
			response.status(400).send(error);
		}
		response.sendStatus(204);
	});
}