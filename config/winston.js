'use strict';

const winston = require('winston'),
  fs = require('fs');

if (!fs.existsSync(process.env.LOGGER_DIRECTORY)) {
  fs.mkdirSync(process.env.LOGGER_DIRECTORY);
}

const customLevels = {
  colors: {
    debug: 'cyan',
    info: 'green',
    warn: 'yellow',
    error: 'red'
  }
};
winston.addColors(customLevels.colors);

new winston.Logger({
  transports: [
    new winston.transports.Console({
    	handleExceptions: true,
			json: true
    }),
    new (require('winston-daily-rotate-file'))({
    	filename: `${process.env.LOGGER_DIRECTORY}/_${process.env.EXCEPTIONS_FILENAME}`,
			handleExceptions: true,
			humanReadableUnhandledException: true,
			datePattern: 'yyyy-MM-dd',
      prepend: true,
    })
  ]
});

module.exports = winston;
