'use strict';

const _ = require('lodash');

const defaultProperties = {
	LOGGER_DIRECTORY: './logs',
	EXCEPTIONS_FILENAME: 'exceptions.log',
	LOGGER_FILENAME: 'app.log',
	LOGGER_LEVEL: 'error',
	ROUTER_LOGGER_FILENAME: 'router.log',
	ROUTER_LOGGER_LEVEL: 'error',
	PORT: 9000
}

_.forIn(defaultProperties, (value, key) => {
	process.env[key] = process.env[key] || value;
	console.log(`Environment property: ${key}:${process.env[key]}`);
});