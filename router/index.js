'use strict';

const router = require('express').Router(),
	helmet = require('helmet'),
	bodyParser = require('body-parser'),
	logger = require('../config/logger').routerLogger;

router.use(helmet());
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));

require('./main.router')(router);
require('./user.router')(router);

router.all('/*', (request, response) => {
	logger.warn(`${request.method} request to ${request.url} not found`);
	response.status(404).send('Request not found');
});

router.use((error, request, response, next) => {
	logger.error(`Error on ${request.method} request to ${request.url} [query: ${JSON.stringify(request.query)}] [body: ${JSON.stringify(request.body)}] --> ${error}`);
	response.status(500).send(error);
});

module.exports = router;
