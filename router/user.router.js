'use strict';

const usersApi = require('../api/users');

module.exports = (router) => {
	router.route('/api/users/:id?')
		.get((request, response, next) => {
			usersApi.getUsers(request, response);
		})
		.post((request, response, next) => {
			usersApi.addUser(request, response);
		})
		.put((request, response, next) => {
			usersApi.updateUser(request, response);
		})
		.delete((request, response, next) => {
			usersApi.deleteUser(request, response);
		});
};